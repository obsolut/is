#!/bin/bash

USERNAME="host"
HOSTNAME="desktop"
PASSWORD="very7strong8password9"

URL="https://download.nus.edu.sg/mirror/gentoo/releases/amd64/autobuilds/current-stage3-amd64-hardened-openrc"
STAGE=$(basename $(wget -q https://download.nus.edu.sg/mirror/gentoo/releases/amd64/autobuilds/latest-stage3-amd64-hardened-openrc.txt -O- | tail -n 1 | cut -d " " -f 1))

read -p 'Encryption password: ' EP
read -p 'Wipe /home? [y/n] ' WIPE

fdisk -W always /dev/sda

if [[ $WIPE == "y" ||  $WIPE == "Y" ]]; then
	fdisk -W always /dev/sdb
	echo -n $EP | cryptsetup luksFormat /dev/sdb1 -d -
fi

echo -n $EP | cryptsetup luksFormat /dev/sda2 -d -
echo -n $EP | cryptsetup open /dev/sda2 root -d -
echo -n $EP | cryptsetup open /dev/sdb1 home -d -

mkfs.vfat -F 32 /dev/sda1
mkfs.ext4 /dev/mapper/root  

if [[ $WIPE == "y" ||  $WIPE == "Y" ]]; then
	mkfs.ext4 /dev/mapper/home
fi

mkdir --parents /mnt/gentoo

mount /dev/mapper/root /mnt/gentoo

cd /mnt/gentoo

wget -q --show-progress $URL/$STAGE

tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner

rm -v stage3-*.tar.xz

cat > /mnt/gentoo/etc/portage/make.conf << EOL
# These settings were set by the catalyst build script that automatically
# built this stage.
# Please consult /usr/share/portage/config/make.conf.example for a more
# detailed example.
COMMON_FLAGS="-march=native -O2 -pipe"
CHOST="x86_64-pc-linux-gnu"
CFLAGS="\${COMMON_FLAGS}"
CXXFLAGS="\${COMMON_FLAGS}"
FCFLAGS="\${COMMON_FLAGS}"
FFLAGS="\${COMMON_FLAGS}"
MAKEOPTS="-j6"

# NOTE: This stage was built with the bindist Use flag enabled
PORTDIR="/var/db/repos/gentoo"
DISTDIR="/var/cache/distfiles"
PKGDIR="/var/cache/binpkgs"

# This sets the language of build output to English.
# Please keep this setting intact when reporting bugs.
LC_MESSAGES=C

USE="elogind minimal -systemd -bluetooth -wifi -gnome -gtk -kde -qt5"

ACCEPT_LICENSE="-* @FREE @BINARY-REDISTRIBUTABLE"

GRUB_PLATFORMS="efi-64"

GENTOO_MIRRORS="https://ftp.jaist.ac.jp/pub/Linux/Gentoo/ https://download.nus.edu.sg/mirror/gentoo/"

EOL

mkdir --parents /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
cp --dereference /etc/resolv.conf /mnt/gentoo/etc/

mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
mount --bind /run /mnt/gentoo/run
mount --make-slave /mnt/gentoo/run

chroot /mnt/gentoo /bin/bash << EOF

source /etc/profile

mount /dev/sda1 /boot
mount /dev/mapper/home /home

emerge-webrsync
emerge --verbose --update --deep --newuse @world

sed -i '24s/.//' /etc/locale.gen
sed -i "s/localhost/$HOSTNAME/g" /etc/conf.d/hostname

locale-gen

eselect locale set 4

env-update && source /etc/profile

emerge sys-kernel/linux-firmware dev-vcs/git app-editors/vim sys-kernel/genkernel sys-kernel/gentoo-kernel-bin net-misc/dhcpcd sys-fs/cryptsetup sys-boot/grub app-admin/sudo

cat >> /etc/fstab << EOL

/dev/sda1               /boot   vfat    defaults        0 2
/dev/mapper/root        /       ext4    defaults        0 1
/dev/mapper/home        /home   ext4    defaults        0 1

EOL

rc-update add dhcpcd default

echo "config_enp34s0="dhcp"" >> /etc/conf.d/net
cd /etc/init.d
ln -s net.lo net.enp34s0
rc-update add net.enp34s0 default
cd

cat >> /etc/conf.d/dmcrypt << EOL

target=home
source=/dev/sdb1
key=/etc/keys/home.key

EOL

mkdir /etc/keys
echo $EP >> /etc/keys/home.key
echo -n $EP | cryptsetup luksAddKey /dev/sdb1 /etc/keys/home.key -d -

sed -i -r 's/#GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="crypt_root=\/dev\/sda2 root=\/dev\/mapper\/root"/g' /etc/default/grub

genkernel --luks initramfs

grub-install --target=x86_64-efi --efi-directory=/boot
grub-mkconfig -o /boot/grub/grub.cfg

rc-update add dmcrypt boot
rc-update add dbus default
rc-update add elogind boot

echo "root:$PASSWORD" | chpasswd

if [ ! -d /home/$USERNAME ]; then
	useradd -m -G users,wheel,audio,video,usb,portage -s /bin/bash $USERNAME
else
	useradd -G users,wheel,audio,video,usb,portage -s /bin/bash $USERNAME
fi

echo "$USERNAME:$PASSWORD" | chpasswd
sed -i '85s/.//' /etc/sudoers

curl https://gitlab.com/obsolut/halo/-/raw/main/etc/local.d/trim.stop -o /etc/local.d/trim.stop

EOF

cd

umount -l /mnt/gentoo/dev{/shm,/pts,}

umount -R /mnt/gentoo

poweroff
